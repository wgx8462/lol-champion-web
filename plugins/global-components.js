import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import lolChampionInfo from '~/components/lol-champion-info'
Vue.component('lolChampionInfo', lolChampionInfo)

